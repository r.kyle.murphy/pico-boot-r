use rp2040_hal as hal;
use hal::gpio::PinId;
use crate::payload::{PayloadError, PayloadRef};
pub use chip::get_and_init_board;
use crate::controller::chip::SleepTrigger;

#[cfg_attr(feature = "rp2040", path = "processor/rp2040.rs")]
#[cfg_attr(feature = "rp2040-pro-micro", path = "processor/rp2040_pro_micro.rs")]
pub(crate) mod chip;

pub trait Controller {
    type CsPin: PinId;
    type ClkPin: PinId;
    type DataPin: PinId;
    type LedPin: PinId;

    fn init() -> Self;
    fn start_hijack(self, payload: PayloadRef) -> SleepTrigger;
    fn signal_error(self, err: PayloadError) -> !;
    fn signal_ready(&mut self);
}