use core::fmt::{Debug};
use rp2040_hal as hal;
use hal::dma::ReadTarget;
use core::ptr;
use core::mem::size_of;
use mutually_exclusive_features::exactly_one_of;
exactly_one_of!("link-ipl-uf2", "include-ipl-bin");

#[cfg_attr(feature = "include-ipl-bin", path = "payload/include_ipl.rs")]
#[cfg_attr(feature = "link-ipl-uf2", path = "payload/link_ipl.rs")]
mod ipl;

const PAYLOAD_MAGIC_0: [u8; 4] = [0x49, 0x50, 0x4C, 0x42];// "IPLB"
const PAYLOAD_MAGIC_1: [u8; 4] = [0x4F, 0x4F, 0x54, 0x20];// "OOT "
const PAYLOAD_MAGIC_2: [u8; 4] = [0x50, 0x49, 0x43, 0x4F]; // "PICO"

pub struct PayloadRef {
    magic0: u32,
    magic1: u32,
    size: u32,
    data: &'static [u8],
}

#[derive(Debug, PartialEq, Eq)]
pub enum PayloadError {
    BadStartMagic0,
    BadStartMagic1,
    BadStartPayload,
    BadSize,
    BadEndMagic,
}

impl PayloadRef {
    pub fn validate(&self) -> Result<usize,PayloadError> {
        if self.magic0.to_be_bytes() != PAYLOAD_MAGIC_0 {
            return Err(PayloadError::BadStartMagic0);
        }
        if self.magic1.to_be_bytes() != PAYLOAD_MAGIC_1 {
            return Err(PayloadError::BadStartMagic1);
        }
        const U32_SIZE: usize = size_of::<u32>();
        if self.size as usize != self.data.len() + (U32_SIZE * 3) {
            return Err(PayloadError::BadSize);
        }

        let size_in_u32 = self.data.len() / size_of::<u32>();
        // 1K chunks
        let alignment = 1024 / size_of::<u32>();
        let size_aligned = (size_in_u32 + alignment - 1) / alignment * alignment;
        //let last_u32_offset = (size_aligned - 1) * size_of::<u32>();
        let last_u32_bytes = self.data.last_chunk::<U32_SIZE>().unwrap();

        if *last_u32_bytes != PAYLOAD_MAGIC_2 {
            return Err(PayloadError::BadEndMagic);
        }

        return Ok(size_aligned);
    }
}

unsafe impl ReadTarget for PayloadRef {
    type ReceivedWord = u32;

    fn rx_treq() -> Option<u8> {
        None
    }

    fn rx_address_count(&self) -> (u32, u32) {
        let size = self.validate().unwrap();
        unsafe {
            (ptr::addr_of!(self.data[0]).add(size_of::<u32>()) as u32, size as u32)
        }
    }

    fn rx_increment(&self) -> bool {
        true
    }
}

pub fn get_payload() -> PayloadRef {
    ipl::get_payload_ref()
}