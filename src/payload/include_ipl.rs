use core::mem::size_of;
use crate::payload::PayloadRef;

static IPLBOOT: &[u8] = include_bytes!("../../iplboot/iplboot.bin");

pub fn get_payload_ref() -> PayloadRef {
    const U32_SIZE: usize = size_of::<u32>();
    let mut chunks = IPLBOOT.as_chunks::<U32_SIZE>().0;
    let magic0 = u32::from_be_bytes(chunks[0]);
    let magic1 = u32::from_be_bytes(chunks[1]);
    let size = u32::from_be_bytes(chunks[2]);
    PayloadRef {
        magic0,
        magic1,
        size,
        data: &IPLBOOT[(3*U32_SIZE)..],
    }
}