use core::marker::PhantomData;

use core::ops::Deref;
use core::ptr::{slice_from_raw_parts};
use crate::payload::PayloadRef;

const IPLBOOT: &IPL = &IPL::take();

pub fn get_payload_ref() -> PayloadRef {
    unsafe {
        PayloadRef {
            magic0: u32::from_be(IPLBOOT.magic0),
            magic1: u32::from_be(IPLBOOT.magic1),
            size: u32::from_be(IPLBOOT.size),
            data: &*slice_from_raw_parts(&IPLBOOT.data_start, u32::from_be(IPLBOOT.size) as usize),
        }
    }
}
struct IPL {
    _marker: PhantomData<*const ()>,
}

#[repr(C)]
struct Payload {
    magic0: u32,
    magic1: u32,
    size: u32,
    data_start: u8,
}

impl IPL {
    const PTR: *const Payload = 0x1008_0000 as *const _;

    const fn take() -> Self {
        IPL {
            _marker: PhantomData
        }
    }
}

impl Deref for IPL {
    type Target = Payload;

    fn deref(&self) -> &Self::Target {
        unsafe { &*Self::PTR }
    }
}
