use embedded_hal::digital::StatefulOutputPin;
use pio::{Instruction, InstructionOperands, MovDestination, MovOperation, MovSource, OutDestination};
use pio_proc::pio_file;
use rp2040_hal as hal;
use hal::pio::{UninitStateMachine,PIOBuilder, Stopped, Tx, StateMachine,PIO,PIOExt, StateMachineIndex, StateMachineGroup2, ValidStateMachine};
use hal::gpio::AnyPin;
use hal::gpio::{OutputDriveStrength, OutputSlewRate, PinId, ValidFunction, Pin, PullNone};
use hal::pio::{Buffers, PinDir, ShiftDirection};
use rp2040_hal::gpio::{FunctionSio, PullDown, SioOutput};
use rp2040_hal::gpio::bank0::Gpio25;

pub struct IPLHijack<P, SM1, SM2, DataPinId, ClkPinId, CsPinId>
where
    DataPinId: PinId + ValidFunction<P::PinFunction>,
    ClkPinId: PinId + ValidFunction<P::PinFunction>,
    CsPinId: PinId + ValidFunction<P::PinFunction>,
    P: PIOExt,
    SM1: StateMachineIndex,
    SM2: StateMachineIndex,
    (P, SM1): ValidStateMachine,
    (P, SM2): ValidStateMachine,
{
    pub clocked_output_tx: Tx<(P, SM2)>,
    on_transfer_tx: Tx<(P, SM1)>,
    on_transfer_sm: StateMachine<(P, SM1), Stopped>,
    clocked_output_sm: StateMachine<(P, SM2), Stopped>,
    data_pin: Pin<DataPinId, P::PinFunction, PullNone>,
    clk_pin: Pin<ClkPinId, P::PinFunction, PullNone>,
    cs_pin: Pin<CsPinId, P::PinFunction, PullNone>,
}

impl<P, SM1, SM2, DataPinId, ClkPinId, CsPinId> IPLHijack<P, SM1, SM2, DataPinId, ClkPinId, CsPinId>
where
    DataPinId: PinId + ValidFunction<P::PinFunction>,
    ClkPinId: PinId + ValidFunction<P::PinFunction>,
    CsPinId: PinId + ValidFunction<P::PinFunction>,
    P: PIOExt,
    SM1: StateMachineIndex,
    SM2: StateMachineIndex,
    (P, SM1): ValidStateMachine,
    (P, SM2): ValidStateMachine,
{
    pub fn new<CsPin, ClkPin, DataPin>(
        cs_pin: CsPin,
        clk_pin: ClkPin,
        data_pin: DataPin,
        pio: &mut PIO<P>,
        sm0: UninitStateMachine<(P, SM1)>,
        sm1: UninitStateMachine<(P, SM2)>,
    ) -> Self
    where
        DataPin: AnyPin<Id =DataPinId>,
        ClkPin: AnyPin<Id =ClkPinId>,
        CsPin: AnyPin<Id =CsPinId>,
    {
        // Setup the GPIO pins the PIOs use
        let mut data_pin = data_pin.into().into_function::<P::PinFunction>().into_pull_type::<PullNone>();
        data_pin.set_slew_rate(OutputSlewRate::Fast);
        data_pin.set_drive_strength(OutputDriveStrength::EightMilliAmps);
        let mut clk_pin = clk_pin.into().into_function::<P::PinFunction>().into_pull_type::<PullNone>();
        clk_pin.set_slew_rate(OutputSlewRate::Fast);
        clk_pin.set_drive_strength(OutputDriveStrength::FourMilliAmps);
        let mut cs_pin = cs_pin.into().into_function::<P::PinFunction>().into_pull_type::<PullNone>();
        cs_pin.set_slew_rate(OutputSlewRate::Fast);
        cs_pin.set_drive_strength(OutputDriveStrength::FourMilliAmps);
        // Assert that the CS pin and CLK pin are consecutive pins
        assert_eq!(cs_pin.id().num, clk_pin.id().num - 1);
        // Load and parse PIO file
        let on_transfer_program = pio_file!("./src/picoboot.pio", select_program("on_transfer"),);
        let clocked_output_program = pio_file!("./src/picoboot.pio", select_program("clocked_output"),);
        let on_transfer_installed = pio.install(&on_transfer_program.program).unwrap();
        let clocked_output_installed = pio.install(&clocked_output_program.program).unwrap();

        // Configure the on_transfer PIO
        let (on_transfer_sm, _, on_transfer_tx) = PIOBuilder::from_installed_program(on_transfer_installed)
            // Read from CS pin
            .jmp_pin(cs_pin.id().num)
            .in_pin_base(cs_pin.id().num)
            // Run at full system clocks
            .clock_divisor_fixed_point(1, 1)
            .build(sm0);
        // Configure the clocked_output PIO
        let (clocked_output_sm, _, clocked_output_tx) = PIOBuilder::from_installed_program(clocked_output_installed)
            .jmp_pin(clk_pin.id().num)
            .in_pin_base(cs_pin.id().num)
            // Having data_pin as both an out and set pin lets us configure it as floating
            .out_pins(data_pin.id().num, 1)
            .set_pins(data_pin.id().num, 1)
            // Configure OSR
            .out_shift_direction(ShiftDirection::Right)
            .autopull(true)
            .pull_threshold(32)
            // We don't use Rx buffer, allocate that for Tx
            .buffers(Buffers::OnlyTx)
            // Run at full system clocks
            .clock_divisor_fixed_point(1, 1)
            .build(sm1);
        // SM Interrupt 0 is used for signaling between the PIO programs
        // SM Interrupt 1 is used to signal when the IPL is finished transferring
        // Map SM interrupt 1 to PIO 0 IRQ 1
        pio.irq1().enable_sm_interrupt(1);
        let mut hijack = IPLHijack {
            clocked_output_tx,
            on_transfer_tx,
            on_transfer_sm,
            clocked_output_sm,
            data_pin,
            clk_pin,
            cs_pin,
        };
        hijack.init_on_transfer();
        hijack.init_clocked_output();
        hijack
    }

    fn init_on_transfer(&mut self) {
        // Set CS pin as input
        self.on_transfer_sm.set_pindirs([(self.cs_pin.id().num, PinDir::Input)]);
        // Setup init state of on_transfer
        //self.on_transfer_tx.write(224); // CS pulses
        self.on_transfer_tx.write(1); // CS pulses
        self.on_transfer_sm.exec_instruction(Instruction {
            operands: InstructionOperands::PULL {
                if_empty: true,
                block: true,
            },
            delay: 0,
            side_set: None,
        });
        self.on_transfer_sm.exec_instruction(Instruction {
            operands: InstructionOperands::MOV {
                destination: MovDestination::X,
                op: MovOperation::None,
                source: MovSource::OSR,
            },
            delay: 0,
            side_set: None,
        });
        self.on_transfer_sm.exec_instruction(Instruction {
            operands: InstructionOperands::OUT {
                destination: OutDestination::NULL,
                bit_count: 32,
            },
            delay: 0,
            side_set: None,
        });
    }

    fn init_clocked_output(&mut self) {
        // Set CS and CLK as input, and DATA as output
        self.clocked_output_sm.set_pindirs([
            (self.cs_pin.id().num, PinDir::Input),
            (self.clk_pin.id().num, PinDir::Input),
            (self.data_pin.id().num, PinDir::Output)
        ]);
        // Setup init state of clocked_output
        self.clocked_output_tx.write(8191); // 8192 bits, 1024 bytes, minus 1 because counting starts from 0
        self.clocked_output_sm.exec_instruction(Instruction {
            operands: InstructionOperands::PULL {
                if_empty: true,
                block: true,
            },
            delay: 0,
            side_set: None,
        });
        self.clocked_output_sm.exec_instruction(Instruction {
            operands: InstructionOperands::MOV {
                destination: MovDestination::Y,
                op: MovOperation::None,
                source: MovSource::OSR,
            },
            delay: 0,
            side_set: None,
        });
        self.clocked_output_sm.exec_instruction(Instruction {
            operands: InstructionOperands::OUT {
                destination: OutDestination::NULL,
                bit_count: 32,
            },
            delay: 0,
            side_set: None,
        });
    }

    pub fn split(self) -> (Tx<(P, SM2)>, StateMachineGroup2<P, SM1, SM2, Stopped>) {
        (self.clocked_output_tx, self.on_transfer_sm.with(self.clocked_output_sm))
    }
}