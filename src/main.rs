#![no_std]
#![no_main]
#![feature(slice_as_chunks)]

use core::cell::RefCell;
use critical_section::Mutex;
//noinspection RsUnusedImport
use panic_halt as _;
use rp2040_hal::entry;
use rp2040_hal::pac::interrupt;
use crate::controller::Controller;
use crate::controller::chip::SleepTrigger;

mod payload;
mod hijack_pio;
mod controller;

static GLOBAL_REF: Mutex<RefCell<Option<SleepTrigger>>> = Mutex::new(RefCell::new(None));

#[entry]
fn main() -> ! {
    let mut board = controller::get_and_init_board();
    //board.signal_ready();
    let payload = payload::get_payload();
    let validation = payload.validate();
    if validation.is_err() {
        board.signal_error(validation.err().unwrap());
    } else {
        board.signal_ready();
    }
    let sleep_trigger = board.start_hijack(payload);
    critical_section::with(|cs| {
        GLOBAL_REF.borrow(cs)
            .replace(Some(sleep_trigger));
    });
    loop {}
}

// Raised when transfer PIO fully transmits the IPL
#[interrupt]
fn PIO0_IRQ_1() {
    critical_section::with(|cs| {
        // Enter deep sleep mode
        if let Some(sleep_trigger) = GLOBAL_REF.borrow(cs).replace(None) {
            sleep_trigger.dormant_sleep();
        }
    })
}