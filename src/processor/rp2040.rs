use rp2040_hal as hal;
use hal::fugit::RateExtU32;
use hal::gpio::bank0::{Gpio25, Gpio4, Gpio5, Gpio6};
use hal::pac;
use hal::clocks::ClocksManager;
use hal::dma::{Channels, DMAExt, Pace};
use hal::dma::single_buffer::Config;
use hal::prelude::*;
use hal::gpio::Pins;
use hal::gpio::{FunctionSio, Pin, PullDown, SioOutput};
use hal::pio::{SM0, SM1, StateMachineGroup2, Stopped, Tx};
use hal::pll::common_configs::{PLL_USB_48MHZ};
use hal::pll::{setup_pll_blocking, PhaseLockedLoop, Locked};
use hal::xosc::{setup_xosc_blocking, Stable};
use pac::{PIO0, PLL_SYS, PLL_USB};
use embedded_hal::delay::DelayNs;
use embedded_hal::digital::{OutputPin, StatefulOutputPin};
use hal::Timer;
use rp2040_hal::fugit::HertzU32;
use rp2040_hal::pll::PLLConfig;
use rp2040_hal::xosc::CrystalOscillator;
use nb::block;
use rp2040_hal::clocks::StoppableClock;

use crate::controller::Controller;
use crate::controller::chip::board::XOSC_CRYSTAL_FREQ;
use crate::hijack_pio::IPLHijack;
use crate::payload::{PayloadError, PayloadRef};

#[cfg_attr(feature = "rp-pico", path = "rp_pico.rs")]
#[cfg_attr(feature = "rp2040-pro-micro", path = "board/rp2040_pro_micro.rs")]
mod board;

pub fn get_and_init_board() -> Rp2040 {
    Rp2040::init()
}

pub struct Rp2040 {
    pub dma: Channels,
    pub tx: Tx<(PIO0, SM1)>,
    pub sm: StateMachineGroup2<PIO0, SM0, SM1, Stopped>,
    pub led_pin: Pin<Gpio25, FunctionSio<SioOutput>, PullDown>,
    pub timer: Timer,
    pub sleep_trigger: SleepTrigger,
}

pub struct SleepTrigger {
    pub xosc: CrystalOscillator<Stable>,
    pub clocks: ClocksManager,
    pub pll_sys: PhaseLockedLoop<Locked, PLL_SYS>,
    pub pll_usb: PhaseLockedLoop<Locked, PLL_USB>,
}

impl SleepTrigger {
    pub fn dormant_sleep(mut self) -> ! {
        block!(self.clocks.system_clock.reset_source_await())
            .unwrap();
        self.clocks.usb_clock.disable();
        self.clocks.adc_clock.disable();
        self.clocks.rtc_clock
            .configure_clock(&self.xosc, 46875u32.Hz())
            .unwrap();
        self.clocks.peripheral_clock
            .configure_clock(&self.clocks.system_clock, self.clocks.system_clock.freq())
            .unwrap();
        self.pll_usb.disable();
        self.pll_sys.disable();
        self.xosc.disable();
        loop {}
    }
}

//const XOSC_CRYSTAL_FREQ: u32 = 12_000_000;

//const OVERCLOCK: u32 = 250_000_000;

impl Rp2040 {
    fn configure_clocks(watchdog: pac::WATCHDOG, timer: pac::TIMER, xosc: pac::XOSC, clocks: pac::CLOCKS, pll_sys: pac::PLL_SYS, pll_usb: pac::PLL_USB, resets: &mut pac::RESETS) -> (SleepTrigger, Timer) {
        let mut watchdog = rp2040_hal::Watchdog::new(watchdog);
        let xosc = setup_xosc_blocking(xosc, XOSC_CRYSTAL_FREQ.Hz()).unwrap();
        watchdog.enable_tick_generation(XOSC_CRYSTAL_FREQ.Hz::<1,1>().to_MHz() as u8);
        let mut clocks = ClocksManager::new(clocks);
        // Slightly overclock the system_clock
        let pll_sys = setup_pll_blocking(pll_sys, xosc.operating_frequency().into(), PLLConfig {
            vco_freq: HertzU32::MHz(1500),
            refdiv: 1,
            post_div1: 6,
            post_div2: 1, // Replace 2 with 1 to double sys clock
        }, &mut clocks, resets).unwrap();
        let pll_usb = setup_pll_blocking(pll_usb, xosc.operating_frequency().into(), PLL_USB_48MHZ, &mut clocks, resets).unwrap();
        let timer = Timer::new(timer, resets, &clocks);
        let sleep_trigger = SleepTrigger {
            xosc,
            clocks,
            pll_sys,
            pll_usb,
        };
        (sleep_trigger, timer)
    }

    fn configure_dma(dma: pac::DMA, resets: &mut pac::RESETS, busctrl: &mut pac::BUSCTRL) -> Channels {
        let channel = dma.split(resets);

        busctrl.bus_priority().write(|w| {
            w.dma_w().set_bit();
            w.dma_r().set_bit()
        });

        channel
    }

    fn blink(&mut self) {
        self.led_pin.set_high().unwrap();
        self.timer.delay_ms(250);
        self.led_pin.set_low().unwrap();
        self.timer.delay_ms(100);
    }
}

impl Controller for Rp2040 {
    type CsPin = Gpio4;
    type ClkPin = Gpio5;
    type DataPin = Gpio6;
    type LedPin = Gpio25;

    fn init() -> Self {
        let mut pac = pac::Peripherals::take().unwrap();
        let _core = pac::CorePeripherals::take().unwrap();

        let (sleep_trigger, timer) = Rp2040::configure_clocks(pac.WATCHDOG, pac.TIMER, pac.XOSC, pac.CLOCKS, pac.PLL_SYS, pac.PLL_USB, &mut pac.RESETS);


        let sio = hal::Sio::new(pac.SIO);
        let pins = Pins::new(
            pac.IO_BANK0,
            pac.PADS_BANK0,
            sio.gpio_bank0,
            &mut pac.RESETS,
        );
        let mut led_pin = pins.gpio25.into_push_pull_output();
        let dma = Rp2040::configure_dma(pac.DMA, &mut pac.RESETS, &mut pac.BUSCTRL);
        let (mut pio0, sm0, sm1, _, _) = pac.PIO0.split(&mut pac.RESETS);

        let ipl_hijack = IPLHijack::new(pins.gpio4, pins.gpio5, pins.gpio6, &mut pio0, sm0, sm1);
        let (tx, sm) = ipl_hijack.split();

        Rp2040 {
            dma,
            tx,
            sm,
            led_pin,
            timer,
            sleep_trigger,
        }
    }

    fn start_hijack(self, payload: PayloadRef) -> SleepTrigger {
        let mut dma_transfer = Config::new(self.dma.ch0, payload, self.tx);
        dma_transfer.pace(Pace::PreferSink);
        dma_transfer.bswap(false);
        dma_transfer.start();

        self.sm.start();
        self.sleep_trigger
    }

    fn signal_error(mut self, error: PayloadError) -> ! {
        loop {
            match error {
                PayloadError::BadStartMagic0 => {
                    // Blink three times
                    self.blink();
                    self.blink();
                    self.blink();
                }
                PayloadError::BadStartMagic1 => {
                    // Blink four times
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                }
                PayloadError::BadSize => {
                    // Blink five times
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                }
                PayloadError::BadEndMagic => {
                    // Blink six times
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                }
                PayloadError::BadStartPayload => {
                    // Blink seven times
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                    self.blink();
                }
            }
            self.timer.delay_ms(3000);
        };
    }

    fn signal_ready(&mut self) {
        self.blink();
        self.blink();
    }
}