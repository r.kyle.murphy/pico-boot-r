use std::error::Error;
use build_pretty::{
    build_pretty,
};
use uf2_decode::convert_from_uf2;

fn main() {
    build_pretty!()
        .enque_fn("Dump UF2 Payload Address ", Box::new(dump_uf2_offset))
    ;
    //     .enque_command("Generate UF2", CommandBuilder::new_with_args("elf2uf2-rs", &[, "pico-boor-r.uf2"]).into());
}

fn dump_uf2_offset(output: &mut String) -> Result<(), Box<dyn Error>>{
    if cfg!(feature = "link-ipl-uf2") {
        let uf2 = convert_from_uf2(include_bytes!("iplboot/iplboot_pico.uf2"))
            .expect("Could not find or convert iplboot/iplboot_pico.uf2");
        *output = format!("Target location of iplboot: {:#08x}", uf2.1.get(&0xe48bff56)
            .expect("iplboot/iplboot_pico.uf2 does not define a payload for RP2040 devices"));
    }
    Ok(())
}