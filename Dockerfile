FROM python:3 AS ipl-extractor
LABEL Name=ipl_extractor

WORKDIR /home/
COPY uf2conv.py uf2families.json ./
COPY iplboot/iplboot_pico.uf2 ./
RUN ["./uf2conv.py", "-f", "RP2040", "-o", "iplboot.bin", "-c", "iplboot_pico.uf2"]
CMD ["bash"]
# RUN mkdir /iplboot
# RUN ["./uf2conv.py", "-f", "RP2040", "-o", "/iplboot/iplboot.bin", "-c", "/iplboot/iplboot_pico.uf2"]